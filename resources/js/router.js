import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

import Home from './components/Home/index';
import ErrorPage from './components/Error/Error404';
import GameMobile from './components/Page/GameMobile.vue';
import Match from './components/Page/Match.vue';
import VideoGame from './components/Page/VideoGame.vue';
import Public from './components/Page/Public.vue';
import Product from './components/Page/product/Product.vue';
import ListProduct from './components/Page/product/ListProduct.vue';
import ProductDetail from './components/Page/product/Detail.vue';
import ProductAdmin from './components/Admin/product/index.vue';

const routes = [
    {
        path: "/",
        component: Home,
        name: 'Home',
    },
    {
        path: "/game-mobile",
        component: GameMobile,
        name: 'GameMobile',
    },
    {
        path: "/match",
        component: Match,
        name: 'Match',
    },
    {
        path: "/video-game",
        component: VideoGame,
        name: 'VideoGame',
    },
    {
        path: "/public",
        component: Public,
        name: 'Public',
    },
    {
        path: '/product',
        name: 'Product',
        component: Product,
        children: [
            {
                path: '',
                name: 'ListProduct',
                component: ListProduct,
            },
            {
                path: ':id',
                name: 'ProductDetail',
                component: ProductDetail,
            }
        ]
    },
    {
        path: "/admin/product",
        component: ProductAdmin,
        name: 'ProductAdmin',
    },
    {
        path: '/error-page-404',
        name: 'ErrorPage',
        component: ErrorPage
    },
    {
        path: '*',
        redirect: '/error-page-404'
    }
]

const router = new VueRouter({
    mode: "history",
    routes,
});

// router.beforeEach((to, from, next) => {
//     const token  = 'hjashajshcjahs';
//     if (!token && to.meta.requiresAuth){
//         next({ name: 'Login' })
//     }else if(to.meta.requiresAuth){
//         next({ name: 'Dashboard' })
//         alert('Ban khong co quyen')
//     }
//     else next()
// })

export default router;
