require('./bootstrap');
window.Vue = require('vue');
import App from "./App";
import router from "./router";
import "ant-design-vue/dist/antd.css";
import Antd from "ant-design-vue";
Vue.use(Antd);

const app = new Vue({
    el: '#app',
    router,
    render: h => h(App),
});
