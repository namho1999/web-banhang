import http from "../../axios/base.config";

class ProductService {
    apiGetProduct() {
        return http.get("/api/v1/list-product");
    }
}
export default new ProductService();
